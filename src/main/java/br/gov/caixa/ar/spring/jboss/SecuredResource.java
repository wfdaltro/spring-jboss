package br.gov.caixa.ar.spring.jboss;

import java.util.Collection;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecuredResource {

	@GetMapping(value = "/secure")
	public ResponseEntity<String> seguro() {
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		return ResponseEntity.ok().body("Usuário: " + name + authorities.size() + authorities);
	}

}
