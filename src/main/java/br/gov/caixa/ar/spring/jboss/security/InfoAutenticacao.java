package br.gov.caixa.ar.spring.jboss.security;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class InfoAutenticacao {
	
	private Collection<GrantedAuthority> authorities;	
	
	private String login;

	public InfoAutenticacao(String username, List<String> roles) {
		login = username;
		 roles.forEach(r->{
			 authorities.add(new SimpleGrantedAuthority(r));
		 });
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Collection<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

}
