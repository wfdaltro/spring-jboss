package br.gov.caixa.ar.spring.jboss.security;

import java.io.Serializable;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

@Component
public class JwtTokenUtil implements Serializable {

	@Value("${jwt.secret}")
	private String publicKey;

	@Value("${jwt.issuer}")
	private String issuer;

	private Claims extractAllClaims(String token) {
		return Jwts.parser().setSigningKey(generatePublicKey()).parseClaimsJws(token).getBody();
	}

	public boolean validateToken(String token) {
		return (!isTokenExpired(token) && isSameIssuer(token));
	}

	private boolean isSameIssuer(String token) {
		return issuer.equals(extractIssuer(token));
	}

	private Boolean isTokenExpired(String token) {
		return extractExpiration(token).before(new Date());
	}

	public Date extractExpiration(String token) {
		return extractClaim(token, Claims::getExpiration);
	}

	public String extractIssuer(String token) {
		return extractClaim(token, Claims::getIssuer);
	}

	public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = extractAllClaims(token);
		return claimsResolver.apply(claims);
	}

	public PublicKey generatePublicKey() {
		try {
			KeyFactory kf = KeyFactory.getInstance("RSA");
			X509EncodedKeySpec pKey = new X509EncodedKeySpec(
					Base64.getDecoder().decode(publicKey));
			return kf.generatePublic(pKey);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
			return null;
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public InfoAutenticacao getAuthenticatedInfo(String token) {
		Claims claims = extractAllClaims(token);
		claims.get("roles");
		String username = claims.get("preferred_username").toString();
		Map realmRoles = (Map) claims.get("realm_access");
		List<String> roles = new ArrayList();
		roles.addAll((List<String>) realmRoles.get("roles"));
		Map resourcesRoles = (Map) claims.get("resource_access");
		resourcesRoles.forEach((key, value) -> {
			Map v = (Map) value;
			roles.addAll((List<String>) v.get("roles"));
		});
		return new InfoAutenticacao(username, roles);

	}

}
